package ru.sberProject.filmLimbrary;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmLimbraryApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(FilmLimbraryApplication.class, args);
    }



    @Override
    public void run(String... args) throws Exception {

    }
}