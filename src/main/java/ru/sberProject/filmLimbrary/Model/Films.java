package ru.sberProject.filmLimbrary.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "films")
public class Films {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "premier_year")
    private Long premierYear;

    @Column(name = "country")
    private String country;

    @Column(name = "genre")
    private Genre genre;

    @ManyToMany
    @JoinTable(name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FK_FILM_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "directors_id"),
            inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILM"))
    private Set<Directors> directors = new HashSet<>();
}
